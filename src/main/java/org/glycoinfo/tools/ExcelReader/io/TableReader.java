package org.glycoinfo.tools.ExcelReader.io;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.glycoinfo.tools.ExcelReader.om.Table;

public class TableReader {

	private int m_iHeaderRow;
	private int m_nColumns;

	private List<Table> m_lTables;

	public TableReader() {
		this.m_iHeaderRow = 3;
		this.m_nColumns = 11;
	}

	public List<Table> getTables() {
		return this.m_lTables;
	}

	public void start(Workbook workbook) {
		this.m_lTables = getTables(workbook, this.m_iHeaderRow, this.m_nColumns);
	}

	/**
	 * Returns list of tables from a workbook with specified header row and number of columns.
	 * @param workbook
	 * @param iHeaderRow
	 * @param nColumns
	 * @return
	 */
	private List<Table> getTables(Workbook workbook, int iHeaderRow, int nColumns) {
		List<Table> tables = new ArrayList<>();

		int sheetCount = workbook.getNumberOfSheets();
		for (int i = 0; i < sheetCount; i++) {
			Sheet sheet = workbook.getSheetAt(i);
			Table table = getTable(sheet, iHeaderRow, nColumns);
			tables.add(table);
		}

		return tables;
	}

	/**
	 * Returns table as a list of data row as a map of header and data values
	 *
	 * @param sheet
	 * @param iHeaderRow
	 * @param nColumns
	 * @return Table
	 */
	private Table getTable(Sheet sheet, int iHeaderRow, int nColumns) {
		Table table = new Table();
		table.setName( sheet.getSheetName() );
		table.setTitle( getTitle(sheet) );

		String[] header = getHeader(sheet, iHeaderRow, nColumns);
		int nRowsTotal = getRowNumberTotal(sheet);
		for (int i = iHeaderRow; i < nRowsTotal; i++) {
			Map<String, Double> dataRow = getDataRow(sheet, header, i);
			table.addRow(dataRow);
		}

		return table;
	}

	private String getTitle(Sheet sheet) {
		return sheet.getRow(0).getCell(0).getStringCellValue();
	}

	/**
	 * Returns header strings.
	 *
	 * @param sheet
	 * @param nHeaderRow
	 * @param nColumns
	 * @return Array of header strings
	 */
	private String[] getHeader(Sheet sheet, int nHeaderRow, int nColumns) {
		String[] header = new String[nColumns];

		for (int i = 0; i < nColumns; i++) {
			header[i] = sheet.getRow(nHeaderRow - 1).getCell(i).getStringCellValue();
		}
		return header;
	}

	/**
	 * Returns data row as a map of header and data values
	 *
	 * @param sheet
	 * @param header
	 * @param nRow
	 * @return Map of header and data value as a data row
	 */
	private Map<String, Double> getDataRow(Sheet sheet, String[] header, int nRow) {
		HashMap<String, Double> mapHeaderToValue = new HashMap<>();

		for (int i = 0; i < header.length; i++) {
			Double value = sheet.getRow(nRow).getCell(i).getNumericCellValue();
			String tempHeader = header[i];
			mapHeaderToValue.put(tempHeader, value);
		}
		return mapHeaderToValue;
	}

	/**
	 * Returns last row number of the sheet
	 *
	 * @param sheet
	 * @return last row number of the sheet
	 */
	private int getRowNumberTotal(Sheet sheet) {
		int rowTotal = sheet.getLastRowNum();

		if ((rowTotal > 0) || (sheet.getPhysicalNumberOfRows() > 0)) {
			rowTotal++;
		}
		return rowTotal;
	}
}
