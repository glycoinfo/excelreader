package org.glycoinfo.tools.ExcelReader.om;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class Table {

	private String name;
	private String title;
	private List<Map<String, Double>> table;

	public Table() {
		this.table = new ArrayList<>();
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public List<Map<String, Double>> getTable() {
		return table;
	}

	public void addRow(Map<String, Double> row) {
		this.table.add(row);
	}

}
